vault_name             = "KVPDigitalCerti01"

secret_name            = "IaCrbacCert01"

client_name            = "IaCrbacdes01-Appid"

k8s_name               = "akspdigitalCrt0100"

k8s_version            = "1.15.10"

agent_count            = "3"

vm_type                = "Standard_B2ms"

resource_group_vault   = "RgVaultDigitalCerti"

rbac_server_app_id     = "AKSAzureADServerCert"

rbac_server_app_secret = "AKSAzureADServerCertSecreto"

rbac_client_app_id     = "AKSAzureADClientCert"

tenant_id              = "Tenant"

resource_group_name    = "RGAksPDigitalCerti"

subnet_name            = "pacificoCert-AKS1"

resource_group_name_sn = "rgPacificoPDigitalCert"

virtual_network_name   = "vNetPacificoADigitalCert"

min_count              = 3

max_count              = 5

network_policy         = null

win_credential         = {
  win_username = {
    user = "azureuser"
  }
}

tags                   = {
  Environment = "Certification"
  Project     = "ArqDigitalCerti"
}
