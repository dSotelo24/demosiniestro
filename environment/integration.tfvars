vault_name             = "KVPDigitalDesa01"

secret_name            = "IaCrbacdes01"

client_name            = "IaCrbacdes01-Appid"

k8s_name               = "AKSPDigitalDes0100"

k8s_version            = "1.15.10"

agent_count            = "2"

vm_type                = "Standard_B2ms"

resource_group_vault   = "RgVaultDigitalDesa"

rbac_server_app_id     = "AKSAzureADServerDes"

rbac_server_app_secret = "AKSAzureADServerDesSecreto"

rbac_client_app_id     = "AKSAzureADClientDes"

tenant_id              = "Tenant"

resource_group_name    = "RGAksPDigitalDesa"

subnet_name            = "pacificoDesa-AKS1"

resource_group_name_sn = "rgPacificoPDigitalDesa"

virtual_network_name   = "vNetPacificoADigitalDesa"

min_count              = 1

max_count              = 3

network_policy         = null

win_credential         = {}

tags                   = {
  Environment = "Integration"
  Project     = "ArqDigitalDesa"
}
