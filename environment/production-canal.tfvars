vault_name             = "KVPDigitalProd01"

secret_name            = "IaCrbacProd01"

client_name            = "IaCrbacprod01-Appid"

k8s_name               = "akscanalprd0100"

k8s_version            = "1.15.10"

agent_count            = "3"

vm_type                = "Standard_D4s_v3"

resource_group_vault   = "RgVaultDigitalProd"

rbac_server_app_id     = "AKSAzureADServerProd"

rbac_server_app_secret = "AKSAzureADServerProdSecreto"

rbac_client_app_id     = "AKSAzureADClientProd"

tenant_id              = "Tenant"

resource_group_name    = "RGAksPDigitalProd"

subnet_name            = "pacificoProd-AKS1"

resource_group_name_sn = "rgPacificoPDigitalProd"

virtual_network_name   = "vNetPacificoPDigitalProd"

min_count              = 3

max_count              = 5

network_policy         = "azure"

win_credential         = {}

tags                   = {
  Environment = "Production"
  Project     = "ArqDigitalProd"
}

os_disk_size_gb        = "30"
