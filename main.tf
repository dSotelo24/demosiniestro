terraform {
  backend "azurerm" {}
}

provider "azurerm" {
  version = "=2.5.0"
  features {}
}

data "azurerm_resource_group" "rg" {
  name  = var.resource_group_name
}


data "azurerm_subnet" "sn" {
  name                 = var.subnet_name
  resource_group_name  = var.resource_group_name_sn
  virtual_network_name = var.virtual_network_name
}

# K8s
module "k8s" {
  source                 = "git::ssh://git@bitbucket.org/pacificoseguros/iac.git//terraform/azure/k8s?ref=v0.12.24"
  resource_group_name    = data.azurerm_resource_group.rg.name
  vault_name             = var.vault_name
  secret_name            = var.secret_name
  client_name            = var.client_name
  k8s_name               = var.k8s_name
  k8s_version            = var.k8s_version
  agent_count            = var.agent_count
  resource_group_vault   = var.resource_group_vault
  subnet                 = data.azurerm_subnet.sn.id
  vm_type                = var.vm_type
  rbac_server_app_id     = var.rbac_server_app_id
  rbac_server_app_secret = var.rbac_server_app_secret
  rbac_client_app_id     = var.rbac_client_app_id
  tenant_id              = var.tenant_id
  min_count              = var.min_count
  max_count              = var.max_count
  network_policy         = var.network_policy
  win_credential         = var.win_credential
  tags                   = var.tags
  os_disk_size_gb        = var.os_disk_size_gb
}
