variable "vault_name" {
}

variable  "secret_name" {
}

variable  "client_name" {
}

variable "k8s_name" {
}

variable "k8s_version" {
}

variable "agent_count" {
}

variable "resource_group_vault" {
}

variable "vm_type" {
}

variable "rbac_server_app_id" {
}

variable "rbac_server_app_secret" {
}

variable "rbac_client_app_id" {
}

variable "tenant_id" {
}

variable "resource_group_name" {
  type        = string
  default     = ""
  description = "(Required) Specifies the Resource Group where the Managed Kubernetes Cluster should exist"
}

variable "resource_group_name_sn" {
  type        = string
  default     = ""
  description = "(Required) Specifies the Resource Group where the Network Subnet should exist"
}

variable "virtual_network_name" {
  type        = string
  default     = ""
  description = "Specifies the name of the Virtual Network this Subnet is located within"
}

variable "subnet_name" {
  type        = string
  default     = ""
  description = "Specifies the name of the Subnet"
}

variable "min_count" {
  type        = number
  default     = 3
}

variable "max_count" {
  type        = number
  default     = 5
  description = ""
}

variable "network_policy" {
  type        = string
  default     = null
  description = ""
}

variable "win_credential" {
  type        = map
  default     = {}
}

variable "tags" {
  type        = map
  default     = {}
}

variable "os_disk_size_gb" {
  type        = string
  default     = "30"
  description = ""
}
